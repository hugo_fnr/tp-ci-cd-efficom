package org.example;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.example.mainobject.FonctionBudgetaire;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void testInitFunctionBudgetaire() {
    	FonctionBudgetaire fb = new FonctionBudgetaire(2.0, 3.0, 1.0, 3, 1.0);
    	List<Double> listCoef = new ArrayList<>();
    	listCoef.add(2.0);
    	listCoef.add(3.0);
    	listCoef.add(1.0);
    	listCoef.add(0.0);
    	listCoef.add(0.0);
    	listCoef.add(0.0);
    	Assert.assertEquals(fb.getListsCoef(), listCoef);
    	String s = "Z = 2.0x1 + 3.0x2 + 1.0x3 + 0.0x4 + 0.0x5 + 0.0x6 + 1.0";
    	Assert.assertEquals(fb.toString(), s);
    	
    }
    
    
}
