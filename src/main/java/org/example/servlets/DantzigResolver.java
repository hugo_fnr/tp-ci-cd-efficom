package org.example.servlets;

import org.example.minimize.FonctionBudgetaireMinimize;
import org.example.minimize.SousContrainteMinimize;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DantzigResolver extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// TO DO LIST : SI MINIMISER AVEC QUE DES CONTRAINTES <= --> RESULTAT Z EST 0
	// SI MAXIMISER AVEC QUE DES CONTRAINTES POSITIVES --> RESULTAT Z EST +INFINI

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession();
		String choixMinMax = req.getParameter("maxmin");

		// DEFINIR VARIABLES JAVA DEPART AVEC VALEUR PAR DEFAUT
		/*
		 * double minimum = 0; List<SousContrainte> listSousContrainte = new
		 * ArrayList<>(); List<Double> listeRatio = new ArrayList<>(); SousContrainte
		 * equationEchangeModif = null; double diviseurEquationEchangeModif = 0; double
		 * coefMultiply = 0; int idxequationSC = 0; int nbSystemContrainte =
		 * Integer.parseInt(req.getParameter("numbercontrainte")); int varEntrante =
		 * -1;/
		 */

		// DEFINIR AU DEBUT - NEW VERSION
		double minimum = 0;
		List<SousContrainteMinimize> listSousContrainte = new ArrayList<>();
		List<Double> listeRatio = new ArrayList<>();
		SousContrainteMinimize equationEchangeModif = null;
		double diviseurEquationEchangeModif = 0;
		int numeroSC = 1;
		double coefMultiply = 0;
		int idxequationSC = 0;
		// NOMBRE DE VARIABLES D'INEQUATION SANS =
		int nbEgal = 0;
		int nbSuperiorOrEqual = 0;
		for (int i = 0; i < Integer.parseInt(req.getParameter("numbercontrainte")); i++) {
			if ((req.getParameter("operator"+ (i + 1))).equals("=")) {
				nbEgal++;
				nbSuperiorOrEqual++;
			}

			if ((req.getParameter("operator"+ (i + 1))).equals(">=")) {
				nbSuperiorOrEqual++;
			}
		}
		int nbSystemContrainte = Integer.parseInt(req.getParameter("numbercontrainte"));
		int nbSystemContrainteNotEqual = Integer.parseInt(req.getParameter("numbercontrainte")) - nbEgal;
		System.out.println("NB EQUATIONS NOT EQUAL : " + nbSystemContrainteNotEqual);
		int nbVariableArtificiels = nbSuperiorOrEqual;
		System.out.println("NB ARTIFICIELLES : " + nbVariableArtificiels);
		int varEntrante = -1;

		// DEFINIR LES OBJETS NECESSAIRES POUR LA MINIMISATION
		List<Double> zj = new ArrayList<Double>();
		List<Double> cjMinuszj = new ArrayList<Double>();
		List<Integer> vdb = new ArrayList<>();
		// TYPE OF REQUEST = true --> MINIMIZATION
		// TYPE OF REQUEST = false --> MAXIMIZATION
		boolean typeOfRequest = true;
		if (choixMinMax.equals("max")) {
			typeOfRequest = false;
		}
		boolean stopProgramme = true;

		// DEFINIR LISTE POUR AVOIR LES DETAILS DE L'ALGORITHME
		int compteur = 0;
		List<Integer> variablesEntrantes = new ArrayList<Integer>();
		List<String> fonctionsBudgetaire = new ArrayList<String>();
		List<List<Double>> ratios = new ArrayList<List<Double>>();
		List<Double> minimums = new ArrayList<Double>();
		List<List<String>> contraintesAll = new ArrayList<List<String>>();
		List<String> equationsEchange = new ArrayList<String>();
		
		//LISTE DETAILS DU NOUVEL ALGORITHME
		List<List<Integer>> idxsVDB = new ArrayList<List<Integer>>();
		List<Double> valeursZ = new ArrayList<Double>();
		List<List<Double>> valeursCP = new ArrayList<List<Double>>();
		List<List<Double>> valeursQTE = new ArrayList<List<Double>>();
		List<String> valeursZJ = new ArrayList<String>();
		List<String> valeursCJMinusZJ = new ArrayList<String>();

		// DEFINIR SYSTEME DE DEPART
		FonctionBudgetaireMinimize fb = new FonctionBudgetaireMinimize(Integer.parseInt(req.getParameter("valuex1")),
				Integer.parseInt(req.getParameter("valuex2")), Integer.parseInt(req.getParameter("valuex3")),
				nbSystemContrainteNotEqual, Integer.parseInt(req.getParameter("sousContrainte")), nbVariableArtificiels,
				typeOfRequest);
		int indexSC = 1;
		for (int i = 0; i <nbSystemContrainte; i++) {
			// ON GERE LES EQUATIONS AVEC LE SIGNE >=
			if (req.getParameter("operator" + (i + 1)).equals(">=")) {
				listSousContrainte
						.add(new SousContrainteMinimize(Integer.parseInt(req.getParameter("contraintex1" + (i + 1))),
								Integer.parseInt(req.getParameter("contraintex2" + (i + 1))),
								Integer.parseInt(req.getParameter("contraintex3" + (i + 1))),
								nbSystemContrainteNotEqual, nbVariableArtificiels, i, indexSC++,
								Integer.parseInt(req.getParameter("contraintevalue" + (i + 1))), 1));
			} else if (req.getParameter("operator" + (i + 1)).equals("<=")) {
				listSousContrainte
						.add(new SousContrainteMinimize(Integer.parseInt(req.getParameter("contraintex1" + (i + 1))),
								Integer.parseInt(req.getParameter("contraintex2" + (i + 1))),
								Integer.parseInt(req.getParameter("contraintex3" + (i + 1))),
								nbSystemContrainteNotEqual, nbVariableArtificiels, i, 0,
								Integer.parseInt(req.getParameter("contraintevalue" + (i + 1))), -1));
			} else {
				listSousContrainte
						.add(new SousContrainteMinimize(Integer.parseInt(req.getParameter("contraintex1" + (i + 1))),
								Integer.parseInt(req.getParameter("contraintex2" + (i + 1))),
								Integer.parseInt(req.getParameter("contraintex3" + (i + 1))),
								nbSystemContrainteNotEqual, nbVariableArtificiels, i, indexSC++,
								Integer.parseInt(req.getParameter("contraintevalue" + (i + 1))), 0));
			}

		}

		session.setAttribute("fonctionZ", fb.toString());
		fonctionsBudgetaire.add(fb.toString());

		String contraintes = "";
		System.out.println(fb.toString());
		for (int i = 0; i < listSousContrainte.size(); i++) {
			System.out.println("(" + (i + 1) + ")" + listSousContrainte.get(i).toString());
			// numeroSC++;
			contraintes += "(" + (i + 1) + ")" + listSousContrainte.get(i).toString() + "<br />";
		}
		session.setAttribute("contraintes", contraintes);

		for (int i = 0; i < fb.getListsCoef().size(); i++) {
			zj.add(0.0);
			cjMinuszj.add(fb.getListsCoef().get(i));
		}
		//POUR POUVOIR AFFICHER INDEX VDB
		List<Integer> lesIndexVDB = new ArrayList<Integer>();
		List<Double> lesValeursCP = new ArrayList<Double>();
		List<Double> lesValeursQTE = new ArrayList<Double>();
		double z = 0;
		for (int i = 0; i < listSousContrainte.size(); i++) {
			listeRatio.add(0.0);
			int idxVDB = getVDB(listSousContrainte.get(i));
			vdb.add(idxVDB);
			lesIndexVDB.add(idxVDB);
			System.out.println("VALUER IDXVDB" + idxVDB);
			lesValeursCP.add(fb.getListsCoef().get(idxVDB));
			lesValeursQTE.add(listSousContrainte.get(i).getSecondMembre());
			System.out.println("VDB SC N°" + (i + 1) + " = " + idxVDB + "  CP = " + fb.getListsCoef().get(idxVDB)
					+ "   QUANTITE =" + listSousContrainte.get(i).getSecondMembre());
			for (int j = 0; j < listSousContrainte.get(i).getListsCoef().size(); j++) {
				zj.set(j, zj.get(j) + fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
				cjMinuszj.set(j, cjMinuszj.get(j)
						- fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
			}
			z += fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getSecondMembre();
		}
		
		//POUR AFFICHER DANS LES DETAILS
		idxsVDB.add(lesIndexVDB);
		valeursZ.add(z);
		valeursCP.add(lesValeursCP);
		valeursQTE.add(lesValeursQTE);
		
		// A METTRE DANS UNE LISTE POUR AFFICHER DANS LES DETAILS RESOLUTIONS

		System.out.println("Z=" + z);
		String monZJ = "ZJ = ";
		for (int i = 0; i < zj.size(); i++) {
			monZJ+=zj.get(i) + "x" + (i + 1) + " + ";
		}
		valeursZJ.add(0, monZJ);
		System.out.println();
		String monCJMZJ = "CJ-ZJ = ";
		for (int i = 0; i < cjMinuszj.size(); i++) {
			monCJMZJ+=cjMinuszj.get(i) + "x" + (i + 1) + " + ";
		}
		valeursCJMinusZJ.add(0, monCJMZJ);
		System.out.println();
		// FIN DE A METTRE DANS UNE LISTE

		// TEST SYMBOLE
		int nbSignContrainte = listSousContrainte.size();

		if (!typeOfRequest) {
			for (int i = 0; i < listSousContrainte.size(); i++) {
				if (listSousContrainte.get(i).getSignEquation() == 1) {
					nbSignContrainte -= 1;
				}
			}
		} else {
			for (int i = 0; i < listSousContrainte.size(); i++) {
				if (listSousContrainte.get(i).getSignEquation() == -1) {
					nbSignContrainte -= 1;
				}
			}
		}
		// RESULTAT A METTRE DANS UNE LISTE POUR AFFICHER DETAILS
		if (nbSignContrainte == 0) {
			stopProgramme = false;
			if (!typeOfRequest) {
				System.out.println("La solution a votre probleme est +inf !");
			} else {
				System.out.println("La solution a votre probleme est 0 !");
			}
		}
		// POUR CA, ON VA AFFECTER UNE VARIABLE SET ATTRIBUTE POUR SESSION
		// SI CELLE CI EST NULL, ON AFFICHE RIEN
		// SI PAS NULL, ON AFFICHE PAS LE BOUTON DÉTAILS ET ON A JUSTE L'AFFICHAGE DU MESSAGE AVEC FONCTION BUDGE ET CONTRAINTES
		//FIN

		while (!isFinish(cjMinuszj, typeOfRequest) && stopProgramme) {
			// CALCUL DE LA VARIABLE ENTRANTE//
			varEntrante = calculMinCjMinuszj(cjMinuszj, typeOfRequest);
	        System.out.println("Variable entrante : " + varEntrante);
			variablesEntrantes.add(varEntrante);

			// CALCUL DES RATIOS//
			List<Double> ratiosBis = new ArrayList<Double>();
			for (int i = 0; i < listSousContrainte.size(); i++) {
				listSousContrainte.get(i).calculRatio(varEntrante);
				listeRatio.set(i, listSousContrainte.get(i).getRatio());
				ratiosBis.add(listSousContrainte.get(i).getRatio());

			}

			ratios.add(ratiosBis);

			minimum = ratioCalcul(listeRatio);
			minimums.add(minimum);

			// CALCUL DE L'EQUATION D'ECHANGE//
			for (int i = 0; i < listSousContrainte.size(); i++) {
				if (minimum == listSousContrainte.get(i).getRatio()) {
					System.out.println("Equation d'echange est " + listSousContrainte.get(i).toString());
					vdb.set(i, varEntrante);
					diviseurEquationEchangeModif = listSousContrainte.get(i).getListsCoef().get(varEntrante);
					equationEchangeModif = listSousContrainte.get(i);
					idxequationSC = i;
					equationsEchange.add(listSousContrainte.get(i).toString());
				}
			}

			for (int i = 0; i < equationEchangeModif.getListsCoef().size(); i++) {
				equationEchangeModif.getListsCoef().set(i,equationEchangeModif.getListsCoef().get(i) / diviseurEquationEchangeModif);
			}

			equationEchangeModif.setSecondMembre(equationEchangeModif.getSecondMembre() / diviseurEquationEchangeModif);
			equationEchangeModif.getListsCoef().set(varEntrante, 1.0);
			listSousContrainte.get(idxequationSC).setListsCoef(equationEchangeModif.getListsCoef());

			// CALCUL DES NOUVEAUX SOUS SYSTEMES CONTRAINTES//
			for (int k = 0; k < listSousContrainte.size(); k++) {
				if (k != idxequationSC) {
					coefMultiply = listSousContrainte.get(k).getListsCoef().get(varEntrante);
					for (int i = 0; i < listSousContrainte.get(k).getListsCoef().size(); i++) {
						if (i != varEntrante) {
							listSousContrainte.get(k).getListsCoef().set(i,
									(listSousContrainte.get(k).getListsCoef().get(i)
											- coefMultiply * equationEchangeModif.getListsCoef().get(i)));
						} else {
							listSousContrainte.get(k).getListsCoef().set(i, 0.0);
						}
					}
					listSousContrainte.get(k).setSecondMembre(listSousContrainte.get(k).getSecondMembre()
							- coefMultiply * equationEchangeModif.getSecondMembre());

				}
			}
			
            //CALCUL DE NOUVEAU ZJ ET CJ_ZJ
            for (int i = 0; i < fb.getListsCoef().size(); i++) {
                zj.set(i, 0.0);
                cjMinuszj.set(i, fb.getListsCoef().get(i));
            }
            z = 0;
            //POUR LE DETAILS DE RESOLUTON
            List<Integer> lesIdxVDB = new ArrayList<Integer>();
            List<Double> lesValCP = new ArrayList<Double>();
            List<Double> lesValQTE = new ArrayList<Double>();
            for (int i = 0; i < listSousContrainte.size(); i++) {
                int idxVDB = vdb.get(i);
                lesIdxVDB.add(idxVDB);
                lesValCP.add(fb.getListsCoef().get(idxVDB));
                lesValQTE.add(listSousContrainte.get(i).getSecondMembre());
                System.out.println("VDB SC N°" + (i + 1) + " = " + idxVDB + "  CP = " + fb.getListsCoef().get(idxVDB) + "   QUANTITE =" + listSousContrainte.get(i).getSecondMembre());
                for (int j = 0; j < listSousContrainte.get(i).getListsCoef().size(); j++) {
                    zj.set(j, zj.get(j) + fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
                    cjMinuszj.set(j, cjMinuszj.get(j) - fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
                }
                z += fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getSecondMembre();
            }
            idxsVDB.add(compteur+1, lesIdxVDB);
            valeursCP.add(compteur+1, lesValCP);
            valeursQTE.add(compteur+1, lesValQTE);
            String monZJbis = "ZJ = ";
            for (int i = 0; i < zj.size(); i++) {
                monZJbis += zj.get(i) + "x" + (i + 1) + " + ";
            }
            valeursZJ.add(compteur+1, monZJbis);
            System.out.println();
            String monCJMinusZJ = "CJ-ZJ = ";
            for (int i = 0; i < cjMinuszj.size(); i++) {
            	monCJMinusZJ += cjMinuszj.get(i) + "x" + (i + 1) + " + ";
            }
            valeursCJMinusZJ.add(compteur+1, monCJMinusZJ);
            System.out.println();
            System.out.println("Z=" + z);
            valeursZ.add(compteur+1, z);
            if (z < 0) {
                System.out.println("VOTRE PROBLEME N'A PAS DE SENS !");
            }


			compteur++;
		}
		//PU UTILE NORMALEMENT
		//session.setAttribute("resultEconomiqueFonction", fb.getSecondMembre());
		session.setAttribute("resultEconomiqueFonction", z);
		session.setAttribute("minmax", choixMinMax);
		session.setAttribute("compteur", compteur);
		session.setAttribute("variablesEntrantes", variablesEntrantes);
		session.setAttribute("fonctionsBudgetaire", fonctionsBudgetaire);
		session.setAttribute("ratios", ratios);
		session.setAttribute("minimums", minimums);
		session.setAttribute("contraintesAll", contraintesAll);
		session.setAttribute("equationsEchange", equationsEchange);
		
		//DETAILS DE L'ALGORITHME
		session.setAttribute("idxsVDB", idxsVDB);
		session.setAttribute("valeurZ", valeursZ);
		session.setAttribute("valeursCP", valeursCP);
		session.setAttribute("valeursQTE", valeursQTE);
		session.setAttribute("valeursZJ", valeursZJ);
		session.setAttribute("valeursCJMinusZJ", valeursCJMinusZJ);
		
		
		
		/*
		 * req.setAttribute("variablesEntrantes", variablesEntrantes);
		 * req.setAttribute("fonctionsBudgetaire", fonctionsBudgetaire);
		 * req.setAttribute("ratios", ratios); req.setAttribute("minimums", minimums);
		 * req.setAttribute("contraintesAll", contraintesAll);
		 * req.setAttribute("equationsEchange", equationsEchange);/
		 */

		res.sendRedirect(res.encodeRedirectURL("dantzigForm.jsp"));

	}

	/**
	 * Methode qui calcul le ratio plus plus petit >0
	 *
	 * @param listeRatio
	 * @return
	 */
	public static double ratioCalcul(List<Double> listeRatio) {
		double[] array = new double[listeRatio.size()];
		for (int i = 0; i < array.length; i++) {

			array[i] = listeRatio.get(i);
		}
		boolean max_val_present = false;

		double min = Integer.MAX_VALUE;

		for (int i = 0; i < array.length; i++) // Loop to find the smallest number in array[]
		{
			if (min > array[i] & array[i] > 0) {
				min = array[i];
			}
			// Edge Case, if all numbers are negative and a MAX value is present
			if (array[i] == Integer.MAX_VALUE) {
				max_val_present = true;
			}
		}

		if (min == Integer.MAX_VALUE && !max_val_present) // no positive value found and Integer.MAX_VALUE
		// is also not present, return -1 as indicator
		{
			return -1;
		}

		return min; // return min positive if -1 is not returned
	}

	public static int getVDB(SousContrainteMinimize sc) {
		for (int i = 3; i < sc.getListsCoef().size(); i++) {
			if (sc.getListsCoef().get(i) > 0) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Methode qui calcul le ratio plus plus petit >0
	 *
	 * @param listeRatio
	 * @return
	 */
	public static int calculMinCjMinuszj(List<Double> cjMinuszj, boolean typeOfRequest) {
		int idxVE = 0;
		if (typeOfRequest) {
			double min = cjMinuszj.get(0);
			for (int i = 1; i < cjMinuszj.size(); i++) {
				if (cjMinuszj.get(i) < 0 && cjMinuszj.get(i) < min) {
					min = cjMinuszj.get(i);
					idxVE = i;
				}
			}
		} else {
			double max = cjMinuszj.get(0);
			for (int i = 1; i < cjMinuszj.size(); i++) {
				if (cjMinuszj.get(i) > 0 && cjMinuszj.get(i) > max) {
					max = cjMinuszj.get(i);
					idxVE = i;
				}
			}
		}

		return idxVE; // return min positive if -1 is not returned
	}

	public static boolean isFinish(List<Double> cjMinuszj, boolean typeOfRequest) {
		if (typeOfRequest) {
			for (int i = 0; i < cjMinuszj.size(); i++) {
				if (cjMinuszj.get(i) < 0) {
					return false;
				}
			}
			return true;
		} else {
			for (int i = 0; i < cjMinuszj.size(); i++) {
				if (cjMinuszj.get(i) > 0) {
					return false;
				}
			}
			return true;
		}
	}

}
