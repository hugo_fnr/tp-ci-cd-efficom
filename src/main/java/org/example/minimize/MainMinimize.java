package org.example.minimize;

import java.util.ArrayList;
import java.util.List;

public class MainMinimize {
    //                                                   System.out.println("");     

    public static void main(String[] args) {

        //DEFINIR AU DEBAT
        double minimum = 0;
        List<SousContrainteMinimize> listSousContrainte = new ArrayList<>();
        List<Double> listeRatio = new ArrayList<>();
        SousContrainteMinimize equationEchangeModif = null;
        double diviseurEquationEchangeModif = 0;
        int numeroSC = 1;
        double coefMultiply = 0;
        int idxequationSC = 0;
        int nbSystemContrainteNotEqual = 4;
        int nbVariableArtificiels = 3;
        int varEntrante = -1;

        //NEW FEATURE 
        List<Double> zj = new ArrayList<Double>();
        List<Double> cjMinuszj = new ArrayList<Double>();
        List<Integer> vdb = new ArrayList<>();
        boolean typeOfRequest = true;
        boolean stopProgramme = true;
        //

        System.out.println("------------------------------SYSTEME DE DEPART---------------------");
// POUR LES > METTRE 1 sinon -1 pour < et 0 pour =
//        //DEFINITION DU SYSTEME DE DEPART//
        FonctionBudgetaireMinimize fb = new FonctionBudgetaireMinimize(5, 4, 6, nbSystemContrainteNotEqual, 0, nbVariableArtificiels, typeOfRequest);
        listSousContrainte.add(new SousContrainteMinimize(1, 1, 1, nbSystemContrainteNotEqual, nbVariableArtificiels, 0, 1, 225, 0)); //IF EQUALS NOT INCREMENT INDEX SOUS CONTRAINT
        listSousContrainte.add(new SousContrainteMinimize(1, 0, 0, nbSystemContrainteNotEqual, nbVariableArtificiels, 1, 2, 45, 1));
        listSousContrainte.add(new SousContrainteMinimize(0, 1, 0, nbSystemContrainteNotEqual, nbVariableArtificiels, 2, 0, 55, -1));
        listSousContrainte.add(new SousContrainteMinimize(0, 0, 1, nbSystemContrainteNotEqual, nbVariableArtificiels, 3, 3, 70, 1));
        listSousContrainte.add(new SousContrainteMinimize(0, 0, 1, nbSystemContrainteNotEqual, nbVariableArtificiels, 4, 0, 100, -1));
        System.out.println(fb.toString());
        for (int i = 0; i < listSousContrainte.size(); i++) {
            System.out.println("(" + numeroSC + ")" + listSousContrainte.get(i).toString());
            numeroSC++;
        }
//CREATE UN PUTAINDE TABLEAU DE MORT !!!!!!!
        for (int i = 0; i < fb.getListsCoef().size(); i++) {
            zj.add(0.0);
            cjMinuszj.add(fb.getListsCoef().get(i));
        }
        double z = 0;
        for (int i = 0; i < listSousContrainte.size(); i++) {
            listeRatio.add(0.0);
            int idxVDB = getVDB(listSousContrainte.get(i));
            vdb.add(idxVDB);
            System.out.println("VDB SC N°" + (i + 1) + " = " + idxVDB + "  CP = " + fb.getListsCoef().get(idxVDB) + "   QUANTITE =" + listSousContrainte.get(i).getSecondMembre());
            for (int j = 0; j < listSousContrainte.get(i).getListsCoef().size(); j++) {
                zj.set(j, zj.get(j) + fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
                cjMinuszj.set(j, cjMinuszj.get(j) - fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
            }
            z += fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getSecondMembre();
        }
        System.out.println("Z=" + z);
        System.out.print("ZJ = ");
        for (int i = 0; i < zj.size(); i++) {
            System.out.print(zj.get(i) + "x" + (i + 1) + " + ");
        }
        System.out.println();
        System.out.print("CJ-ZJ = ");
        for (int i = 0; i < cjMinuszj.size(); i++) {
            System.out.print(cjMinuszj.get(i) + "x" + (i + 1) + " + ");
        }
        System.out.println();

        //TEST SYMBOLE
        int nbSignContrainte = listSousContrainte.size();

        if (!typeOfRequest) {
            for (int i = 0; i < listSousContrainte.size(); i++) {
                if (listSousContrainte.get(i).getSignEquation() == 1) {
                    nbSignContrainte -= 1;
                }
            }
        } else {
            for (int i = 0; i < listSousContrainte.size(); i++) {
                if (listSousContrainte.get(i).getSignEquation() == -1) {
                    nbSignContrainte -= 1;
                }
            }
        }
        if (nbSignContrainte == 0) {
            stopProgramme = false;
            if (!typeOfRequest) {
                System.out.println("La solution a votre probleme est +inf !");
            } else {
                System.out.println("La solution a votre probleme est 0 !");
            }
        }

        while (!isFinish(cjMinuszj, typeOfRequest) && stopProgramme) {
            System.out.println("------------------------------DEBUT ITERATION---------------------");

            //CALCUL DE LA VARIABLE ENTRANTE//
            varEntrante = calculMinCjMinuszj(cjMinuszj, typeOfRequest);
            System.out.println("Variable entrante : " + varEntrante);

            //CALCUL DES RATIOS//
            for (int i = 0; i < listSousContrainte.size(); i++) {
                listSousContrainte.get(i).calculRatio(varEntrante);
                listeRatio.set(i, listSousContrainte.get(i).getRatio());
                System.out.println("Ratio " + i + " : " + listSousContrainte.get(i).getRatio());
            }
            minimum = ratioCalcul(listeRatio);
            System.out.println("MINIMUN = " + minimum);
            //CALCUL DE L'EQUATION D'ECHANGE//
            for (int i = 0; i < listSousContrainte.size(); i++) {
                if (minimum == listSousContrainte.get(i).getRatio()) {
                    vdb.set(i, varEntrante);
                    System.out.println("Equation d'echange est " + listSousContrainte.get(i).toString());
                    diviseurEquationEchangeModif = listSousContrainte.get(i).getListsCoef().get(varEntrante);
                    equationEchangeModif = listSousContrainte.get(i);
                    idxequationSC = i;
                }
            }
            for (int i = 0; i < equationEchangeModif.getListsCoef().size(); i++) {
                equationEchangeModif.getListsCoef().set(i, equationEchangeModif.getListsCoef().get(i) / diviseurEquationEchangeModif);
            }
            equationEchangeModif.setSecondMembre(equationEchangeModif.getSecondMembre() / diviseurEquationEchangeModif);
            equationEchangeModif.getListsCoef().set(varEntrante, 1.0);
            listSousContrainte.get(idxequationSC).setListsCoef(equationEchangeModif.getListsCoef());

            //CALCUL DES NOUVEAUX SOUS SYSTEMES CONTRAINTES//
            for (int k = 0; k < listSousContrainte.size(); k++) {
                if (k != idxequationSC) {
                    coefMultiply = listSousContrainte.get(k).getListsCoef().get(varEntrante);
                    for (int i = 0; i < listSousContrainte.get(k).getListsCoef().size(); i++) {
                        if (i != varEntrante) {
                            listSousContrainte.get(k).getListsCoef().set(i, (listSousContrainte.get(k).getListsCoef().get(i) - coefMultiply * equationEchangeModif.getListsCoef().get(i)));
                        } else {
                            listSousContrainte.get(k).getListsCoef().set(i, 0.0);
                        }
                    }
                    listSousContrainte.get(k).setSecondMembre(listSousContrainte.get(k).getSecondMembre() - coefMultiply * equationEchangeModif.getSecondMembre());
                    System.out.println("(" + numeroSC + ")" + listSousContrainte.get(k).toString());
                } else {
                    System.out.println("(" + numeroSC + ")" + listSousContrainte.get(k).toString());
                }
                numeroSC++;
            }

            //CALCUL DE NOUVEAU ZJ ET CJ_ZJ
            for (int i = 0; i < fb.getListsCoef().size(); i++) {
                zj.set(i, 0.0);
                cjMinuszj.set(i, fb.getListsCoef().get(i));
            }
            z = 0;
            for (int i = 0; i < listSousContrainte.size(); i++) {
                int idxVDB = vdb.get(i);
                System.out.println("VDB SC N°" + (i + 1) + " = " + idxVDB + "  CP = " + fb.getListsCoef().get(idxVDB) + "   QUANTITE =" + listSousContrainte.get(i).getSecondMembre());
                for (int j = 0; j < listSousContrainte.get(i).getListsCoef().size(); j++) {
                    zj.set(j, zj.get(j) + fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
                    cjMinuszj.set(j, cjMinuszj.get(j) - fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getListsCoef().get(j));
                }
                z += fb.getListsCoef().get(idxVDB) * listSousContrainte.get(i).getSecondMembre();
            }
            System.out.print("ZJ = ");
            for (int i = 0; i < zj.size(); i++) {
                System.out.print(zj.get(i) + "x" + (i + 1) + " + ");
            }
            System.out.println();
            System.out.print("CJ-ZJ = ");
            for (int i = 0; i < cjMinuszj.size(); i++) {
                System.out.print(cjMinuszj.get(i) + "x" + (i + 1) + " + ");
            }
            System.out.println();
            System.out.println("Z=" + z);
            if (z < 0) {
                System.out.println("VOTRE PROBLEME N'A PAS DE SENS !");
            }
            System.out.println("------------------------------FIN ITERATION --------------------------------");
        }
    }

    /**
     * Methode qui calcul le ratio plus plus petit >0
     *
     * @param listeRatio
     * @return
     */
    public static double ratioCalcul(List<Double> listeRatio) {
        double[] array = new double[listeRatio.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = listeRatio.get(i);
        }
        boolean max_val_present = false;

        double min = Integer.MAX_VALUE;

        for (int i = 0; i < array.length; i++) // Loop to find the smallest number in array[]
        {
            if (min > array[i] && array[i] > 0) {
                min = array[i];
            }
            //Edge Case, if all numbers are negative and a MAX value is present
            if (array[i] == Integer.MAX_VALUE) {
                max_val_present = true;
            }
        }

        if (min == Integer.MAX_VALUE && !max_val_present) //no positive value found and Integer.MAX_VALUE 
        //is also not present, return -1 as indicator
        {
            return -1;
        }

        return min; //return min positive if -1 is not returned
    }

    /**
     * Methode qui calcul le ratio plus plus petit >0
     *
     * @param listeRatio
     * @return
     */
    public static int calculMinCjMinuszj(List<Double> cjMinuszj, boolean typeOfRequest) {
        int idxVE = 0;
        if (typeOfRequest) {
            double min = cjMinuszj.get(0);
            for (int i = 1; i < cjMinuszj.size(); i++) {
                if (cjMinuszj.get(i) < 0 && cjMinuszj.get(i) < min) {
                    min = cjMinuszj.get(i);
                    idxVE = i;
                }
            }
        } else {
            double max = cjMinuszj.get(0);
            for (int i = 1; i < cjMinuszj.size(); i++) {
                if (cjMinuszj.get(i) > 0 && cjMinuszj.get(i) > max) {
                    max = cjMinuszj.get(i);
                    idxVE = i;
                }
            }
        }

        return idxVE; //return min positive if -1 is not returned
    }

    public static int getVDB(SousContrainteMinimize sc) {
        for (int i = 3; i < sc.getListsCoef().size(); i++) {
            if (sc.getListsCoef().get(i) > 0) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isFinish(List<Double> cjMinuszj, boolean typeOfRequest) {
        if (typeOfRequest) {
            for (int i = 0; i < cjMinuszj.size(); i++) {
                if (cjMinuszj.get(i) < 0) {
                    return false;
                }
            }
            return true;
        } else {
            for (int i = 0; i < cjMinuszj.size(); i++) {
                if (cjMinuszj.get(i) > 0) {
                    return false;
                }
            }
            return true;
        }
    }
}
