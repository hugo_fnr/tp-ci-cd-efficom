package org.example.backup;
import java.util.ArrayList;
import java.util.List;

public class Main {
    //                                                   System.out.println("");     

    public static void main(String[] args) {

        //DEFINIR AU DEBAT
        double minimum = 0;
        List<SousContrainte> listSousContrainte = new ArrayList<>();
        List<Double> listeRatio = new ArrayList<>();
        SousContrainte equationEchangeModif = null;
        double diviseurEquationEchangeModif = 0;
        int numeroSC = 1;
        double coefMultiply = 0;
        int idxequationSC = 0;
        int nbSystemContrainte = 3;
        int varEntrante = -1;

        System.out.println("------------------------------SYSTEME DE DEPART---------------------");

        //DEFINITION DU SYSTEME DE DEPART//
        FonctionBudgetaire fb = new FonctionBudgetaire(3, 2, nbSystemContrainte, 0);
        listSousContrainte.add(new SousContrainte(2, 1, 3, 1, 18));
        listSousContrainte.add(new SousContrainte(2, 3, 3, 2, 42));
        listSousContrainte.add(new SousContrainte(3, 1, 3, 3, 24));

        System.out.println(fb.toString());
        for (int i = 0; i < listSousContrainte.size(); i++) {
            System.out.println("(" + numeroSC + ")" + listSousContrainte.get(i).toString());
            numeroSC++;
        }

        while (!fb.isFinish()) {
            System.out.println("------------------------------DEBUT ITERATION---------------------");

            //CALCUL DE LA VARIABLE ENTRANTE//
            varEntrante = fb.findVariableEntrante();
            System.out.println("Variable entrante : " + varEntrante);

            //CALCUL DES RATIOS//
            for (int i = 0; i < listSousContrainte.size(); i++) {
                listSousContrainte.get(i).calculRatio(varEntrante);
                listeRatio.add(listSousContrainte.get(i).getRatio());
                System.out.println("Ratio " + i + " : " + listSousContrainte.get(i).getRatio());
            }
            minimum = ratioCalcul(listeRatio);

            //CALCUL DE L'EQUATION D'ECHANGE//
            for (int i = 0; i < listSousContrainte.size(); i++) {
                if (minimum == listSousContrainte.get(i).getRatio()) {
                    System.out.println("Equation d'echange est " + listSousContrainte.get(i).toString());
                    diviseurEquationEchangeModif = listSousContrainte.get(i).getListsCoef().get(varEntrante);
                    equationEchangeModif = listSousContrainte.get(i);
                    idxequationSC = i;
                }
            }
            for (int i = 0; i < equationEchangeModif.getListsCoef().size(); i++) {
                equationEchangeModif.getListsCoef().set(i, equationEchangeModif.getListsCoef().get(i) / diviseurEquationEchangeModif);
            }
            equationEchangeModif.setSecondMembre(equationEchangeModif.getSecondMembre() / diviseurEquationEchangeModif);
            equationEchangeModif.getListsCoef().set(varEntrante, 1.0);
            listSousContrainte.get(idxequationSC).setListsCoef(equationEchangeModif.getListsCoef());

            //CALCUL DES NOUVEAUX SOUS SYSTEMES CONTRAINTES//
            for (int k = 0; k < listSousContrainte.size(); k++) {
                if (k != idxequationSC) {
                    coefMultiply = listSousContrainte.get(k).getListsCoef().get(varEntrante);
                    for (int i = 0; i < listSousContrainte.get(k).getListsCoef().size(); i++) {
                        if (i != varEntrante) {
                            listSousContrainte.get(k).getListsCoef().set(i, (listSousContrainte.get(k).getListsCoef().get(i) - coefMultiply * equationEchangeModif.getListsCoef().get(i)));
                        } else {
                            listSousContrainte.get(k).getListsCoef().set(i, 0.0);
                        }
                    }
                    listSousContrainte.get(k).setSecondMembre(listSousContrainte.get(k).getSecondMembre() - coefMultiply * equationEchangeModif.getSecondMembre());
                    System.out.println("(" + numeroSC + ")" + listSousContrainte.get(k).toString());
                } else {
                    System.out.println("(" + numeroSC + ")" + listSousContrainte.get(k).toString());
                }
                numeroSC++;
            }

            //CALCUL DE LA NOUVELLE FONCTION BUDGETAIRE//
            coefMultiply = fb.getListsCoef().get(varEntrante);
            for (int i = 0; i < fb.getListsCoef().size(); i++) {
                if (i != varEntrante) {
                    fb.getListsCoef().set(i, fb.getListsCoef().get(i) - coefMultiply * equationEchangeModif.getListsCoef().get(i));
                } else {
                    fb.getListsCoef().set(i, 0.0);
                }
            }
            fb.setSecondMembre(fb.getSecondMembre() + coefMultiply * equationEchangeModif.getSecondMembre());
            System.out.println(fb.toString());

            System.out.println("------------------------------FIN ITERATION --------------------------------");
        }
        System.out.println("Le resultat est " + fb.secondMembre);
    }

    /**
     * Methode qui calcul le ratio plus plus petit >0
     *
     * @param listeRatio
     * @return
     */
    public static double ratioCalcul(List<Double> listeRatio) {
        double[] array = new double[listeRatio.size()];
        for (int i = 0; i < array.length; i++) {

            array[i] = listeRatio.get(i);
        }
        boolean max_val_present = false;

        double min = Integer.MAX_VALUE;

        for (int i = 0; i < array.length; i++) // Loop to find the smallest number in array[]
        {
            if (min > array[i] && array[i] > 0) {
                min = array[i];
            }
            //Edge Case, if all numbers are negative and a MAX value is present
            if (array[i] == Integer.MAX_VALUE) {
                max_val_present = true;
            }
        }

        if (min == Integer.MAX_VALUE && !max_val_present) //no positive value found and Integer.MAX_VALUE 
        //is also not present, return -1 as indicator
        {
            return -1;
        }

        return min; //return min positive if -1 is not returned
    }
}
