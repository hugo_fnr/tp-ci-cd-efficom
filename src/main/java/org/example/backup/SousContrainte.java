package org.example.backup;
import java.util.ArrayList;
import java.util.List;

public class SousContrainte {

    private List<Double> listsCoef;
    private double secondMembre;
    private double ratio;

    public SousContrainte(double coef1, double coef2, int nbcoef, int numSC, double secondMembre) {
        this.listsCoef = new ArrayList<>();
        listsCoef.add(coef1);
        listsCoef.add(coef2);
        for (int i = 0; i < nbcoef; i++) {
            listsCoef.add(0.0);
        }
        listsCoef.set(numSC + 1, 1.0);
        this.secondMembre = secondMembre;
    }

    public List<Double> getListsCoef() {
        return listsCoef;
    }

    public void setListsCoef(List<Double> listsCoef) {
        this.listsCoef = listsCoef;
    }

    public double getSecondMembre() {
        return secondMembre;
    }

    public void setSecondMembre(double secondMembre) {
        this.secondMembre = secondMembre;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    @Override
    public String toString() {
        return "" + listsCoef.get(0) + "x1 + " + listsCoef.get(1) + "x2 + " + listsCoef.get(2) + "x3 + " + listsCoef.get(3) + "x4 + " + listsCoef.get(4) + "x5" + " = " + secondMembre;
    }

    /**
     * METHODE QUI CALCUL LE RATIO DU SOUS SYSTEME
     *
     * @param variableEntrante
     * @return
     */
    public double calculRatio(int variableEntrante) {
        this.ratio = getSecondMembre() / getListsCoef().get(variableEntrante);
        return this.ratio;
    }
}
