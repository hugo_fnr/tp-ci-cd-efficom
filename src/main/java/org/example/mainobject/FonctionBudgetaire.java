package org.example.mainobject;
import java.util.ArrayList;
import java.util.List;

public class FonctionBudgetaire {

    List<Double> listsCoef;
    double secondMembre;

    public FonctionBudgetaire(double coef1, double coef2, double coef3, int nbcoef, double secondMembre) {
        this.listsCoef = new ArrayList<>();
        listsCoef.add(coef1);
        listsCoef.add(coef2);
        listsCoef.add(coef3);
        for (int i = 0; i < nbcoef; i++) {
            listsCoef.add(0.0);
        }
        this.secondMembre = secondMembre;
    }

    public List<Double> getListsCoef() {
        return listsCoef;
    }

    public void setListsCoef(List<Double> listsCoef) {
        this.listsCoef = listsCoef;
    }

    public double getSecondMembre() {
        return secondMembre;
    }

    public void setSecondMembre(double secondMembre) {
        this.secondMembre = secondMembre;
    }

    @Override
    public String toString() {
    	String s = "Z = ";
    	for ( int i =0; i < listsCoef.size(); i++) {
    		s = s + listsCoef.get(i) + "x" + (i+1) + " + ";
    	}
    	s+=""+secondMembre;
        return s;
        		//"Z = " + listsCoef.get(0) + "x1 + " + listsCoef.get(1) + "x2 + " + listsCoef.get(2) + "x3 + " + listsCoef.get(3) + "x4 + " + listsCoef.get(4) + "x5 + "+ listsCoef.get(5) + "x6 +" + secondMembre;
    }

    public boolean isFinish() {
        for (int i = 0; i < this.getListsCoef().size(); i++) {
            if (this.getListsCoef().get(i) > 0) {
                return false;
            }
        }
        return true;
    }

    public int findVariableEntrante() {
        double max = this.getListsCoef().get(0);
        int idx = 0;
        for (int i = 0; i < this.getListsCoef().size(); i++) {
            if (this.getListsCoef().get(i) > 0 && this.getListsCoef().get(i) > max) {
                max = this.getListsCoef().get(i);
                idx = i;
            }
        }
        return idx;
    }
}
