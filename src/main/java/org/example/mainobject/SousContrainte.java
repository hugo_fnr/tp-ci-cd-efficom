package org.example.mainobject;
import java.util.ArrayList;
import java.util.List;

public class SousContrainte {

    private List<Double> listsCoef;
    private double secondMembre;
    private double ratio;

    public SousContrainte(double coef1, double coef2, double coef3, int nbcoef, int numSC, double secondMembre) {
        this.listsCoef = new ArrayList<>();
        listsCoef.add(coef1);
        listsCoef.add(coef2);
        listsCoef.add(coef3);
        for (int i = 0; i < nbcoef; i++) {
            listsCoef.add(0.0);
        }
        listsCoef.set(numSC + 2, 1.0);
        this.secondMembre = secondMembre;
    }

    public List<Double> getListsCoef() {
        return listsCoef;
    }

    public void setListsCoef(List<Double> listsCoef) {
        this.listsCoef = listsCoef;
    }

    public double getSecondMembre() {
        return secondMembre;
    }

    public void setSecondMembre(double secondMembre) {
        this.secondMembre = secondMembre;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    @Override
    public String toString() {
    	String s = "";
    	for ( int i =0; i < listsCoef.size() -1; i++) {
    		s = s + listsCoef.get(i) + "x" + (i+1) + " + ";
    	}
    	s += listsCoef.get(listsCoef.size()-1) + "x" + (listsCoef.size());
    	s+=" = " +secondMembre;
        return s;
        //return "" + listsCoef.get(0) + "x1 + " + listsCoef.get(1) + "x2 + " + listsCoef.get(2) + "x3 + " + listsCoef.get(3) + "x4 + " + listsCoef.get(4) + "x5 +" + listsCoef.get(5) + "x6" + " = " + secondMembre;
    }

    /**
     * METHODE QUI CALCUL LE RATIO DU SOUS SYSTEME
     *
     * @param variableEntrante
     * @return
     */
    public double calculRatio(int variableEntrante) {
        this.ratio = getSecondMembre() / getListsCoef().get(variableEntrante);
        return this.ratio;
    }
}
