<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page session="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dantzig Form</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
</head>
<body class="is-preload">
	<header id="header">
		<a class="logo" href="index.jsp">Dantzig Algorithm Resolver 2020 </a>
		<nav>
			<a href="#menu">Menu</a>
		</nav>
	</header>

	<!-- Nav -->
	<nav id="menu">
		<ul class="links">
			<li><a href="index.html">Home</a></li>
			<li><a href="dantzigForm.jsp"> Go to the resolver </a></li>
			<li><a href=".html"> About us </a></li>
		</ul>
	</nav>


	<h1>Remplissez le formulaire suivant</h1>


	<div class="content">

		<form action=servlet-resolve method=post>

			<div class="form-example">
				<label for="select-maxmin">Choisissez si vous souhaitez
					maximiser ou minimiser : </label> <select name="maxmin" id="maxmin-select"
					required="required">
					<option value="">--Selectionner votre choix--</option>
					<option value="max">max</option>
					<option value="min">min</option>
				</select>
			</div>

			<div class="form-example">
				<label for="economique">Entrez la fonction economique : </label> <input
					type="number" value="0" name="sousContrainte" required="required">
				+ <input type="number" name="valuex1" value="0" required="required">
				x1 + <input type="number" name="valuex2" value="0"
					required="required"> x2 +<input type="number"
					name="valuex3" value="0" required="required"> x3
			</div>

			<div class="form-example">
				<label for="contrainte-select">Choisissez votre nombre de
					contrainte :</label> <select name="numbercontrainte"
					id="numbercontrainte-select" required="required">
					<option value="">--Selectionner votre choix --</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</div>
			<div id="container"></div>

			<br /> <input type="submit" value="Resolve">

		</form>

		<hr>

		<c:if
			test="${ !empty sessionScope.minmax && !empty sessionScope.resultEconomiqueFonction }">
			<p>
				Le ${ sessionScope.minmax } de la fonction economique ${ sessionScope.fonctionZ }
				<br /> ${ sessionScope.contraintes } saisie est : ${ sessionScope.resultEconomiqueFonction }
			</p>
			<p> 
				<br /> Nombre d'iterations : ${ sessionScope.compteur }
			</p>

			<div>
				<form method=post action=details.jsp >
					<input type=submit value="Voir les détails de la résolution">
				</form>

			</div>
		</c:if>




	</div>

</body>
<!-- Footer -->
<footer id="footer">
	<div class="inner">
		<div class="content">
			<section>
				<h3>Regarding creative commons license</h3>
				<p>
					Thanks to &copy; Untitled. Photos <a href="https://unsplash.co">Unsplash</a>,
					Video <a href="https://coverr.co">Coverr</a> for the template.
				</p>
			</section>
			<section>
				<h4>Purpose</h4>
				<p>Project made at the CNAM de Lille during the RPC101
					instruction module</p>
				<p>More informations below :</p>
				<ul class="alt">
					<li><a target="_blank"
						href="http://www.cnam-npdc.org/formations/formations.php">
							CNAM de Lille </a></li>
					<li><a target="_blank"
						href="http://formation.cnam.fr/rechercher-par-discipline/recherche-operationnelle-et-aide-a-la-decision-208739.kjsp">
							RP101 Instruction module </a></li>
				</ul>
			</section>
			<section>
				<h4>Follow Us</h4>
				<ul class="plain">
					<li><a target="_blank"
						href="https://www.linkedin.com/in/steven-copin-99653114b/"><i
							class="icon fa-linkedin">&nbsp;</i>Steven's LinkedIn</a></li>
					<li><a target="_blank"
						href="https://www.linkedin.com/in/hugo-fournier-a2453114b/"><i
							class="icon fa-linkedin">&nbsp;</i>Hugo's LinkedIn</a></li>
				</ul>
			</section>
		</div>
		<div class="copyright">Made by Steven Copin &amp; Hugo Fournier
			- &copy; 2019 All Rights Reserved;</div>
	</div>
</footer>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
	var e = document.getElementById("numbercontrainte-select");
	e.onchange = function() {
		var number = e.options[e.selectedIndex].text;
		var container = document.getElementById("container");

		while (container.hasChildNodes()) {
			container.removeChild(container.lastChild);
		}

		for (i = 0; i < number; i++) {
			container.appendChild(document.createTextNode("Contrainte "
					+ (i + 1) + ": "));
			var input = document.createElement("input");
			input.type = "number";
			input.value = 0;
			input.name = "contraintex1" + (i + 1);
			input.required = "required";
			container.appendChild(input);
			container.appendChild(document.createTextNode(" x1 +"));
			var input2 = document.createElement("input");
			input2.type = "number";
			input2.value = 0;
			input2.name = "contraintex2" + (i + 1);
			input2.required = "required";
			container.appendChild(input2);
			container.appendChild(document.createTextNode("x2 +"));
			var input3 = document.createElement("input");
			input3.type = "number";
			input3.value = 0;
			input3.name = "contraintex3" + (i + 1);
			input3.required = "required";
			container.appendChild(input3);
			container.appendChild(document.createTextNode("x3"));
			//Create array of options to be added
			var array = [ "<=","=",">=" ];
			var select = document.createElement("select");
			select.id = "operatorcontrainte";
			select.name = "operator" + (i + 1);
			container.appendChild(select);
			for (var j = 0; j < array.length; j++) {
				var option = document.createElement("option");
				option.value = array[j];
				option.text = array[j];
				select.appendChild(option);
			}

			var input4 = document.createElement("input");
			input4.type = "number";
			input4.value = 0;
			input4.name = "contraintevalue" + (i + 1);
			input4.required = "required";
			container.appendChild(input4);
			container.appendChild(document.createElement("br"));
		}

	}
</script>
</html>