<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page session="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dantzig Form</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
</head>
<body class="is-preload">
	<header id="header">
		<a class="logo" href="index.jsp">Dantzig Algorithm Resolver 2020 </a>
		<nav>
			<a href="#menu">Menu</a>
		</nav>
	</header>

	<!-- Nav -->
	<nav id="menu">
		<ul class="links">
			<li><a href="index.html">Home</a></li>
			<li><a href="dantzigForm.jsp"> Go to the resolver </a></li>
			<li><a href="aboutUs.html"> About us </a></li>
		</ul>
	</nav>


	<h1>Détails des étapes de l'algorithme</h1>


	<div class="content">
		<c:if test="${ !empty sessionScope.fonctionZ }">

			<%
				List<Integer> variablesEntrantes = new ArrayList<Integer>();
					variablesEntrantes = (List<Integer>) session.getAttribute("variablesEntrantes");

					List<List<Double>> ratios = new ArrayList<List<Double>>();
					ratios = (List<List<Double>>) session.getAttribute("ratios");

					List<Double> minimums = new ArrayList<Double>();
					minimums = (List<Double>) session.getAttribute("minimums");

					List<String> equationsEchange = new ArrayList<String>();
					equationsEchange = (List<String>) session.getAttribute("equationsEchange");

					List<String> fonctionsBudgetaire = new ArrayList<String>();
					fonctionsBudgetaire = (List<String>) session.getAttribute("fonctionsBudgetaire");

					List<List<String>> contraintesAll = new ArrayList<List<String>>();
					contraintesAll = (List<List<String>>) session.getAttribute("contraintesAll");

					//DETAILS NOUVEAU ALGORITHME
					List<List<Integer>> idxsVDB = new ArrayList<List<Integer>>();
					idxsVDB = (List<List<Integer>>) session.getAttribute("idxsVDB");
					List<Double> valeursZ = new ArrayList<Double>();
					valeursZ = (List<Double>) session.getAttribute("valeurZ");
					List<List<Double>> valeursCP = new ArrayList<List<Double>>();
					valeursCP = (List<List<Double>>) session.getAttribute("valeursCP");
					List<List<Double>> valeursQTE = new ArrayList<List<Double>>();
					valeursQTE = (List<List<Double>>) session.getAttribute("valeursQTE");
					List<String> valeursZJ = new ArrayList<String>();
					valeursZJ = (List<String>) session.getAttribute("valeursZJ");
					List<String> valeursCJMinusZJ = new ArrayList<String>();
					valeursCJMinusZJ = (List<String>) session.getAttribute("valeursCJMinusZJ");
			%>

			<p>
				On cherche à ${ sessionScope.minmax } le résultat de la fonction <br />
				Fonction économique ${ sessionScope.fonctionZ } <br /> ${ sessionScope.contraintes }
			</p>

			<%
				for (int i = 0; i <= (Integer) session.getAttribute("compteur"); i++) {
			%>


			<%
				if (i == 0) {
			%>
			<p>
				<%
					for (int idx = 0; idx < idxsVDB.get(i).size(); idx++) {
				%>
				VALEUR INDEX VARIABLE DANS BASE -->
				<%=idxsVDB.get(i).get(idx)%>
				VARIABLE DANS BASE SOUS CONTRAINTE N°1 =
				<%=idxsVDB.get(i).get(idx)%>
				| CP =
				<%=valeursCP.get(i).get(idx)%>
				| QUANTITE =
				<%=valeursQTE.get(i).get(idx)%>
				<br />
				<%
					}
				%>
				Valeurs Z =
				<%=valeursZ.get(i)%>
				<br /> ZJ =
				<%= valeursZJ.get(i) %>
				<br /> CJ-ZJ =
				<%= valeursCJMinusZJ.get(i) %><br />




			</p>
			<%  } else if (i == (Integer) session.getAttribute("compteur")) { %>
			<p>

				Variable entrante :
				<%= variablesEntrantes.get(i-1) %>
				<br />
				<% for ( int idxRatio = 0; idxRatio < ratios.get(i -1).size(); idxRatio++) { %>
				Ratio
				<%= i %>
				:
				<%= ratios.get(i-1).get(idxRatio) %>
				<br />
				<% } %>
				MINIMUM =
				<%= minimums.get(i-1) %>
				Equation d'echange :
				<%= equationsEchange.get(i-1) %>
				<br />
				<%
					for (int idx = 0; idx < idxsVDB.get(i).size(); idx++) {
				%>
				VALEUR INDEX VARIABLE DANS BASE -->
				<%=idxsVDB.get(i).get(idx)%>
				VARIABLE DANS BASE SOUS CONTRAINTE N°1 =
				<%=idxsVDB.get(i).get(idx)%>
				| CP =
				<%=valeursCP.get(i).get(idx)%>
				| QUANTITE =
				<%=valeursQTE.get(i).get(idx)%>
				<br />
				<%
					}
				%>
				ZJ =
				<%= valeursZJ.get(i) %>
				<br /> CJ-ZJ =
				<%= valeursCJMinusZJ.get(i) %><br /> Valeurs Z =
				<%=valeursZ.get(i)%>
				<br />




			</p>	
			
			
			

			<%
				} else {
			%>

			<p>

				Variable entrante :
				<%= variablesEntrantes.get(i-1) %>
				<br />
				<% for ( int idxRatio = 0; idxRatio < ratios.get(i-1).size(); idxRatio++) { %>
				Ratio
				<%= i %>
				:
				<%= ratios.get(i-1).get(idxRatio) %>
				<br />
				<% } %>
				MINIMUM =
				<%= minimums.get(i-1) %>
				Equation d'echange :
				<%= equationsEchange.get(i-1) %>
				<br />
				<%
					for (int idx = 0; idx < idxsVDB.get(i).size(); idx++) {
				%>
				VALEUR INDEX VARIABLE DANS BASE -->
				<%=idxsVDB.get(i).get(idx)%>
				VARIABLE DANS BASE SOUS CONTRAINTE N°1 =
				<%=idxsVDB.get(i).get(idx)%>
				| CP =
				<%=valeursCP.get(i).get(idx)%>
				| QUANTITE =
				<%=valeursQTE.get(i).get(idx)%>
				<br />
				<%
					}
				%>
				ZJ =
				<%= valeursZJ.get(i) %>
				<br /> CJ-ZJ =
				<%= valeursCJMinusZJ.get(i) %><br /> Valeurs Z =
				<%=valeursZ.get(i)%>
				<br />




			</p>




			<%
				}
			%>

			<hr>

			<%
				}
			%>





		</c:if>


		<form method=post action=dantzigForm.jsp>
			<%
				session.invalidate();
			%>
			<input type=submit value="Retour au formulaire">
		</form>


	</div>

</body>
<!-- Footer -->
<footer id="footer">
	<div class="inner">
		<div class="content">
			<section>
				<h3>Regarding creative commons license</h3>
				<p>
					Thanks to &copy; Untitled. Photos <a href="https://unsplash.co">Unsplash</a>,
					Video <a href="https://coverr.co">Coverr</a> for the template.
				</p>
			</section>
			<section>
				<h4>Purpose</h4>
				<p>Project made at the CNAM de Lille during the RPC101
					instruction module</p>
				<p>More informations below :</p>
				<ul class="alt">
					<li><a target="_blank"
						href="http://www.cnam-npdc.org/formations/formations.php">
							CNAM de Lille </a></li>
					<li><a target="_blank"
						href="http://formation.cnam.fr/rechercher-par-discipline/recherche-operationnelle-et-aide-a-la-decision-208739.kjsp">
							RP101 Instruction module </a></li>
				</ul>
			</section>
			<section>
				<h4>Follow Us</h4>
				<ul class="plain">
					<li><a target="_blank"
						href="https://www.linkedin.com/in/steven-copin-99653114b/"><i
							class="icon fa-linkedin">&nbsp;</i>Steven's LinkedIn</a></li>
					<li><a target="_blank"
						href="https://www.linkedin.com/in/hugo-fournier-a2453114b/"><i
							class="icon fa-linkedin">&nbsp;</i>Hugo's LinkedIn</a></li>
				</ul>
			</section>
		</div>
		<div class="copyright">Made by Steven Copin &amp; Hugo Fournier
			- &copy; 2019 All Rights Reserved;</div>
	</div>
</footer>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
	var e = document.getElementById("numbercontrainte-select");
	e.onchange = function() {
		var number = e.options[e.selectedIndex].text;
		var container = document.getElementById("container");

		while (container.hasChildNodes()) {
			container.removeChild(container.lastChild);
		}

		for (i = 0; i < number; i++) {
			container.appendChild(document.createTextNode("Contrainte "
					+ (i + 1) + ": "));
			var input = document.createElement("input");
			input.type = "number";
			input.name = "contraintex1" + (i + 1);
			input.required = "required";
			container.appendChild(input);
			container.appendChild(document.createTextNode(" x1 + "));
			var input2 = document.createElement("input");
			input2.type = "number";
			input2.name = "contraintex2" + (i + 1);
			input2.required = "required";
			container.appendChild(input2);
			container.appendChild(document.createTextNode("x2"));
			//Create array of options to be added
			var array = [ "<=","=",">=" ];
			var select = document.createElement("select");
			select.id = "operatorcontrainte";
			select.name = "operator" + (i + 1);
			container.appendChild(select);
			for (var j = 0; j < array.length; j++) {
				var option = document.createElement("option");
				option.value = array[j];
				option.text = array[j];
				select.appendChild(option);
			}

			var input3 = document.createElement("input");
			input3.type = "number";
			input3.name = "contraintevalue" + (i + 1);
			input3.required = "required";
			container.appendChild(input3);
			container.appendChild(document.createElement("br"));
		}

	}
</script>
</html>